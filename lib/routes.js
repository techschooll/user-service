const UserService = require('./service/user-service')
const Schema = require('../lib/schema/user-schema')
const BaseHelper = require('base-lib').Helper
const Config = require('../config/service-config')

const authenticate = async (request, h) => {
  try {
    let credentials = await new BaseHelper({
      Config
    }).authenticateRequest(request.headers["x-jwt"], request, Config, Config.routes.auth[request._route.path])
    return true
  } catch (e) {
    throw e
  }
}

const routes = [{
  method: 'GET',
  path: '/v1/user/fetchroles',
  config: {
    tags: ['api'],
    pre: [{
      method: authenticate
    }]
  },
  handler: async function(req, h) {
    let userServiceObj = new UserService(req.headers["x-request-context"], req.headers["x-internal-credentials"])
    return await userServiceObj.fetchRoles()
  }
}, {
  method: 'GET',
  path: '/v1/user/getjwt/{user_id}',
  handler: async function(req, h) {
    let userServiceObj = new UserService(req.headers["x-request-context"], req.headers["x-internal-credentials"])
    return await userServiceObj.getJwt(req.params)
  },
  config: {
    tags: ['api'],
    validate: {
      params: Schema.getJwtParamsSchema
    },
    response: {
      schema: Schema.getJwtResponseSchema,
    }
  }
}, {
  method: 'POST',
  path: '/v1/user/create-user',
  handler: async function(req, h) {
    let userServiceObj = new UserService(req.headers["x-request-context"], req.headers["x-internal-credentials"])
    return await userServiceObj.createUser(req.payload)
  },
  config: {
    tags: ['api'],
    validate: {
      payload: Schema.createUserPayloadSchema
    },
    response: {
      schema: Schema.userSchema
    }
  }
}, {
  method: 'DELETE',
  path: '/v1/user/remove-user/{id}',
  handler: async function(req, h) {
    let userServiceObj = new UserService(req.headers["x-request-context"], req.headers["x-internal-credentials"])
    return await userServiceObj.removeUser(req.params)
  },
  config: {
    tags: ['api'],
    pre: [{
      method: authenticate
    }],
    validate: {
      params: Schema.removeUserParamsSchema
    }
  }
}, {
  method: 'POST',
  path: '/v1/user/update-user-role',
  handler: async function(req, h) {
    let userServiceObj = new UserService(req.headers["x-request-context"], req.headers["x-internal-credentials"])
    return await userServiceObj.updateUserRole(req.payload)
  },
  config: {
    tags: ['api'],
    validate: {
      payload: Schema.updateUserRoleSchema
    }
  }
},{
  method: 'POST',
  path: '/v1/user/update-user',
  config: {
    tags: ['api']
  },
  handler: async function(req, h) {
    let userServiceObj = new UserService(req.headers["x-request-context"], req.headers["x-internal-credentials"])
    return await userServiceObj.updateUser(req.payload)
  }
},{
  method: 'DELETE',
  path: '/v1/user/remove-user-role/{user_id}/{role_id}',
  handler: async function(req, h) {
    let userServiceObj = new UserService(req.headers["x-request-context"], req.headers["x-internal-credentials"])
    return await userServiceObj.removeUserRole(req.params)
  },
  config: {
    tags: ['api'],
    pre: [{
      method: authenticate
    }],
    validate: {
      params: Schema.removeUserRoleSchema
    }
  }
}]

module.exports = routes;