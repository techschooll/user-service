const RoleAdapter = require('../../data-access/adapter/role-adapter')
const Config = require('../../config/service-config')
const UserAdapter = require('../../data-access/adapter/user-adapter')
const UserRoleAdapter = require('../../data-access/adapter/user-role-adapter')
const Jwt = require('jsonwebtoken')
const Uuid = require('uuid')
const _ = require('lodash')

class UserService {
  constructor(requestContext, credentials) {
    if(requestContext) requestContext = JSON.parse(requestContext)
    if(credentials) credentials = JSON.parse(credentials)
    requestContext.config = Config
    requestContext.credentials = credentials
    this.requestContext = requestContext
    this.roleAdapter = new RoleAdapter(requestContext)
    this.userAdapter = new UserAdapter(requestContext)
    this.userRoleAdapter = new UserRoleAdapter(requestContext)
  }

  //private functions go here.. 
  async _asyncForEach(array = [], callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }

  async _removeUserRoles(payload) {
    await this._asyncForEach(payload, async (record) => {
      await this.userRoleAdapter.deleteUserRole(record.id)
    })
  }

  //public methods go here.. 

  async fetchRoles() {
    return await this.roleAdapter.fetchAllRoles()
  }

  async getJwt(params) {
    try {
      let user = await this.userAdapter.fetchUserById(params.user_id)
      let userRoles = await this.userRoleAdapter.fetchRolesByUserId(params.user_id)
      let roleCodes = [], rolePrevileges = []
      await this._asyncForEach(userRoles, async (userrolerecord) => {
        let roleDefinition = await this.roleAdapter.fetchRoleById(userrolerecord.role_id)
        roleCodes = roleCodes.concat([roleDefinition.role_key])
        rolePrevileges = rolePrevileges.concat(roleDefinition.previleges)
      })
      let jwtResponse = Jwt.sign({
        data: {
          user_id: _.get(user, `id`),
          session_id: Uuid.v4(),
          roles: roleCodes,
          previleges: rolePrevileges,
          courses: _.get(user, `courses`)
        }
      }, _.get(this, `requestContext.config.jwt.secret`), {
        expiresIn: _.get(this, `requestContext.config.jwt.expiry`)
      })
      return {
        jwt: jwtResponse
      }
    } catch (e) {
      throw e
    }
  }

  async updateUserRole(payload, user) {
    try {
      if(!user) {
        user = {}
        _.set(user, 'id', _.get(payload, `user_id`))
      }
      await this._asyncForEach(payload.roles, async (role) => {
        let roleId = await this.roleAdapter.getRoleByKey(role)
        roleId = _.get(roleId, '[0].id')
        let roleRecord = await this.userRoleAdapter.insertUserRole({
          user_id: user.id,
          role_id: roleId
        })
      })
      return {
        "user_id": user
      }
    }
    catch(e) {
      throw e
    }
  }

  async createUser(payload) {
    try {
      let userPayload = _.omit(payload, ['roles'])
      let user = await this.userAdapter.insertUser(userPayload)
      await this.updateUserRole(payload, user)
      return user
    }
    catch(e) {
      throw e
    }
  }

  async removeUser(payload) {
    try {
      let removedUser = await this.userAdapter.deleteUser(payload.id)
      let userRoleRecordsToRemove = await this.userRoleAdapter.fetchRolesByUserId(payload.id)
      await this._removeUserRoles(userRoleRecordsToRemove)
      return {
        "user_id": payload.id
      }
    }
    catch(e) {
      throw e
    }
  }

  async removeUserRole(payload) {
    try {
      let roleId = await this.roleAdapter.getRoleByKey(payload.role_id)
      roleId = _.get(roleId, '[0].id')
      let recordsToRemove = await this.userRoleAdapter.fetchRolesByUserIdAndRoles(payload.user_id, roleId)
      await this._removeUserRoles(recordsToRemove)
      return {
        "user_id": payload.user_id
      }
    }
    catch(e) { 
      console.log(e)
      throw e
    }
  }

  async updateUser(payload) {
    try {
      let response = await this.userAdapter.updateUser(payload)
      return response
    }
    catch(e) {
      throw e
    }
  }
}


module.exports = UserService;