const joi = require('@hapi/joi')

const getJwtParamsSchema = joi.object({
  user_id: joi.string().required()
})

const getJwtResponseSchema = joi.object({
  jwt: joi.string().required()
})

const userSchema = joi.object({
  id: joi.string().email().required(),
  is_active: joi.boolean().optional(),
  address: joi.object().optional(),
  user_type: joi.string().optional(),
  data: joi.string().optional(),
  courses: joi.array().optional()
})

const uerRoleSchema = joi.object({
  user_id: joi.string().email().required(),
  role_id: joi.number().required()
})

const createUserPayloadSchema = userSchema.keys({
  roles: joi.array().optional()
})

const removeUserParamsSchema = joi.object({
  id: joi.string().email().required()
})

const updateUserRoleSchema = joi.object({
  user_id: joi.string().email().required(),
  roles: joi.array().required()
})

const removeUserRoleSchema = joi.object({
  user_id: joi.string().email().required(),
  role_id: joi.string().required()
})

const localExports = {
  getJwtParamsSchema,
  getJwtResponseSchema,
  userSchema,
  createUserPayloadSchema,
  uerRoleSchema,
  removeUserParamsSchema,
  updateUserRoleSchema,
  removeUserRoleSchema
}

module.exports = localExports