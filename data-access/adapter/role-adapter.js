const BaseAdapter = require('base-lib').BaseAdapter;
const _ = require('lodash')

class RoleAdapter extends BaseAdapter {
  constructor(requestContext) {
    super(requestContext)
    this.requestContext = requestContext
    this.tableName = 'roles'
    this.connectionstring = _.get(this, `requestContext.config.db.connectionstring`,"")+ this.tableName
  }

  async fetchAllRoles() {
    let response = await this.get(this.connectionstring)
    return response.data
  }

  async fetchRoleById(roleId) {
    let response = await this.get(this.connectionstring + "/" + roleId)
    return response.data
  }

  async getRoleByKey(roleKey) {
    let response = await this.get(this.connectionstring + "?role_key=" + roleKey)
    return response.data
  }

} 

module.exports = RoleAdapter