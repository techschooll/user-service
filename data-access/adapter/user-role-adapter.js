const BaseAdapter = require('base-lib').BaseAdapter;
const _ = require('lodash')

class UserRoleAdapter extends BaseAdapter {
  constructor(requestContext) {
    super(requestContext)
    this.requestContext = requestContext
    this.tableName = 'user_roles'
    this.connectionString = _.get(this, `requestContext.config.db.connectionstring`, "") + this.tableName
  }

  async fetchRolesByUserId(userId) {
    try {
      let response = await this.get(this.connectionString + "/?user_id=" + userId)
      return response.data
    } catch (e) {
      throw e
    }
  }

  async insertUserRole(payload) {
    try {
      let response = await this.post(this.connectionString, payload)
      return response.data
    }
    catch(e) {
      throw e
    }
  }

  async deleteUserRole(payload) {
    try {
      let response = await this.delete(this.connectionString + "/" + payload)
      return response.data
    } catch (e) {
      throw e
    }
  }

  async fetchRolesByUserIdAndRoles(userId, roleId) {
    try {
      let response = await this.get(this.connectionString + "/?user_id=" + userId + "&role_id=" + roleId)
      return response.data
    } catch (e) {
      throw e
    }
  }
}

module.exports = UserRoleAdapter