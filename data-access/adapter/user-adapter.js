const BaseAdapter = require('base-lib').BaseAdapter;
const _ = require('lodash')

class UserAdapter extends BaseAdapter {
  constructor(requestContext) {
    super(requestContext)
    this.requestContext = requestContext
    this.tableName = 'users'
    this.connectionString = _.get(this, `requestContext.config.db.connectionstring`, "") + this.tableName
  }

  async fetchUserById(userId) {
    try {
      let response = await this.get(this.connectionString + "/" + userId)
      return response.data
    } catch (e) {
      throw e
    }
  }

  async insertUser(payload) {
    try {
      let response = await this.post(this.connectionString, payload)
      return response.data
    } catch (e) {
      throw e
    }
  }

  async deleteUser(userId) {
    try {
      let response = await this.delete(this.connectionString + "/" + userId)
      return response.data
    } catch (e) {
      throw e
    }
  }

  async updateUser(payload) {
    try {
      let response = await this.put(this.connectionString + "/" + payload.id, payload)
      return response.data
    } catch (e) {
      throw e
    }
  }

}

module.exports = UserAdapter