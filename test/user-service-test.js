const assert = require('chai').assert;
const UserService = require('../lib/service/user-service')
const Config = require('../config/service-config')
const uuid = require('uuid')

describe('User Service Tests', async function() {
    let requestContext = JSON.stringify({
        config: Config
    })
    let userServiceObj = new UserService(requestContext)
    it('should return valid JWT for an existing user', async function() {
        try {
            let response = await userServiceObj.getJwt({
                user_id: "author1@techschool.in"
            })
            assert.exists(response.jwt)
        } catch (e) {
            throw e
        }
    })

    it('fetch all roles in the service', async function() {
        try {
            let response = await userServiceObj.fetchRoles()
            assert.equal(response.length, 3)
            assert.equal(response[0].role_name, "admin")
        } catch (e) {
            throw e
        }
    })
    let createUserId = uuid.v4()
    it('create a new user in the system', async function() {
        try {
            let response = await userServiceObj.createUser({
                "id": createUserId + "@abc.com",
                "is_active": true,
                "roles": ["AUT"]
            })
            assert.equal(response.id, createUserId + "@abc.com")
        } catch (e) {
            throw e
        }
    })

    it('error is thrown when same user is created again in the system', async function() {
        try {
            let response = await userServiceObj.createUser({
                "id": createUserId + "@abc.com",
                "is_active": true,
                "roles": ["AUT"]
            })
            throw "this should not be thrown"
        } catch (e) {
            assert.equal(e.response.data.indexOf("Error: Insert failed, duplicate id"), 0)
        }
    })

    it('update the role to admin for the newly created user', async function() {
        try {
            await userServiceObj.updateUserRole({
                "user_id": createUserId + "@abc.com",
                "roles": ["ADM"]
            })
            let response = await userServiceObj.userRoleAdapter.fetchRolesByUserIdAndRoles(createUserId + "@abc.com", 1)
            assert.equal(response[0].user_id, createUserId + "@abc.com")
            assert.equal(response[0].role_id, 1)
        } catch (e) {
            throw e
        }
    })

    it('remove the role that was mapped to the user', async function() {
        try {
            await userServiceObj.removeUserRole({
                "user_id": createUserId + "@abc.com",
                "role_id": "ADM"
            })
            let response = await userServiceObj.userRoleAdapter.fetchRolesByUserIdAndRoles(createUserId + "@abc.com", 1)
            assert.equal(response.length, 0)
        } catch (e) {
            throw e
        }
    })

    it('update attributes of the newly created user', async function() {
        try {
            await userServiceObj.updateUser({

                "id": createUserId + "@abc.com",
                "is_active": true,
                "courses": [{
                    "id": 1,
                    "status": "inprogress",
                    "resume_info": {
                        "section_id": 1,
                        "lesson_id": 1,
                        "resume_time": "00:10:12"
                    }
                }]
            })
            let response = await userServiceObj.userAdapter.fetchUserById(createUserId + "@abc.com")
            assert.equal(response.courses[0].status, "inprogress")
        } catch (e) {
            throw e
        }
    })

    it('remove the newly created user', async function() {
        try {
            await userServiceObj.removeUser({
                "id": createUserId + "@abc.com"
            })
            let response = await userServiceObj.userAdapter.fetchUserById(createUserId + "@abc.com")

        } catch (e) {
            //checking for 404 since email ID is the primary key and 
            //search by primary key yields 404 if record is not found in json-server
            assert.equal(e.response.status, 404)
        }
    })
})