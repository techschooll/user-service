let config = {
  "server": {
    "port": 3400,
    "host": "localhost"
  },
  "service": "user-service",
  "db": {
    "connectionstring": "http://localhost:3000/"
  },
  "jwt": {
    "expiry": "1h",
    "secret": "c4929650-9241-4f46-9643-6297e686d8f7"
  },
  "routes": {
    "auth": {
      "/v1/user/fetchroles": ["ADM"],
      "/v1/user/create-user": ["ADM"],
      "/v1/user/remove-user/{id}": ["ADM"],
      "/v1/user/remove-user-role/{user_id}/{role_id}": ["ADM"]
    }
  }
}

module.exports = config