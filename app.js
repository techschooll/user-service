const BaseAppLoader = require('base-lib').BaseAppLoader;
const Config = require('./config/service-config');
const Dependencies = {}
const _ = require('lodash')
const Routes = require('./lib/routes');


class App extends BaseAppLoader {
  constructor(config, dependencies) {
    super(config, dependencies)
    super.startServer()
  }

  async registerRoutes(config) {
    _.forEach(Routes, (route) => {
      this.server.route(route);
    })
  }
}

var app = new App(Config, Dependencies)